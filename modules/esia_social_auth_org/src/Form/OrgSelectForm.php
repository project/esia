<?php

namespace Drupal\esia_social_auth_org\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Select organizations.
 */
class OrgSelectForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $organizations = []): array {
    $form['organizations'] = [
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#options' => $organizations,
      '#title' => $this->t('Select organizations for authorization'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'esia_social_auth_org_select';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $organizations = implode(',', $values['organizations']);

    $url = Url::FromRoute('',['orgs' => $organizations]);
    $form_state->setRedirectUrl($url);
  }

}
