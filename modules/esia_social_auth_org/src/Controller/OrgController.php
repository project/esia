<?php

namespace Drupal\esia_social_auth_org\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\esia_social_auth\Controller\SocialAuthController;
use Drupal\esia_social_auth_org\Form\OrgSelectForm;
use Drupal\esia_social_auth_org\OrgInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;


/**
 * Gosuslusgi controller class.
 */
class OrgController extends SocialAuthController implements ContainerInjectionInterface {

  /**
   * Organization service.
   */
  protected Org $org;

  /**
   * OAuth2ControllerBase constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              LoggerChannelFactoryInterface $logger,
                              MessengerInterface $messenger,
                              NetworkManager $network_manager,
                              UserAuthenticator $user_authenticator,
                              RequestStack $request,
                              SocialAuthDataHandler $data_handler,
                              RendererInterface $renderer,
                              EventDispatcherInterface $dispatcher,
                              Org $org
    ) {
    parent::__construct($config_factory, $logger, $messenger,
      $network_manager, $user_authenticator, $request, $data_handler,
      $renderer, $dispatcher
    );
    $this->org = $org;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler'),
      $container->get('renderer'),
      $container->get('event_dispatcher'),
      $container->get('esia_social_auth_org.service')
    );
  }

  /*
   * Organization request.
   */
  public function orgSelect(): array {
    $orgs = [];
    $output['form'] = $this->formBuilder->getForm(OrgSelectForm::class, $orgs);

    return $output;
  }

  /**
   * Organization request.
   */
  public function orgRequest(): Response {
    $context = new RenderContext();

    /** @var \Drupal\Core\Routing\TrustedRedirectResponse|\Symfony\Component\HttpFoundation\RedirectResponse $response */
    $response = $this->renderer->executeInRenderContext($context, function () {

      $url = $this->org->getUnapprovedOrgUrl($this->currentUser()->id());

      if ($url) {
        return new TrustedRedirectResponse($url);
      }
      else {
        return $this->redirect('user.login');
      }
    });

    // Add bubbleable metadata to the response.
    if ($response instanceof TrustedRedirectResponse && !$context->isEmpty()) {
      $bubbleable_metadata = $context->pop();
      $response->addCacheableDependency($bubbleable_metadata);
    }

    return $response;
  }

  /**
   * Callback response router handler for networks.
   */
  public function callbackOrg(): RedirectResponse {
    $config = $this->configFactory->get('esia_social_auth_org.settings');

    if ($config->get('orgRenewEvent')) {
      $this->callbackGosuslugi();
      $url = Url::fromRoute('esia_social_auth_org.org_select');
      return new RedirectResponse($url->toString());
    }

    return parent::callbackGosuslugi();
  }

}
