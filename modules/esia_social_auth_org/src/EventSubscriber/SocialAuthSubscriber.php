<?php

namespace Drupal\esia_social_auth_org\EventSubscriber;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Url;
use Drupal\social_auth\Event\LoginEvent;
use Drupal\social_auth\Event\SocialAuthEvents;
use Drupal\social_auth\Event\UserEvent;
use Drupal\esia_social_auth_org\OrgInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Reacts on Social Auth events.
 */
class SocialAuthSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   */
  private ConfigFactory $configFactory;

  /**
   * SocialAuthSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Used for accessing configuration object factory.
   */
  public function __construct(
    ConfigFactory $config_factory
  ) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[SocialAuthEvents::USER_LOGIN] = ['onUserLogin'];
    $events[SocialAuthEvents::USER_CREATED] = ['onUserCreated'];

    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function onUserCreated(UserEvent $event): void {
    $config = $this->configFactory->get('esia_social_auth_org.settings');

    if ($config->get('orgStatus') == OrgInterface::ORG_RENEW_EVENT_REGISTER) {

      // Bad way, but class LoginEvent hasn't "redirect":
      $url = Url::fromRoute('esia_social_auth_org.request');
      $redirect = new RedirectResponse($url->toString());
      $redirect->send();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onUserLogin(LoginEvent $event): void {
    $config = $this->configFactory->get('esia_social_auth_org.settings');

    if ($config->get('orgStatus') == OrgInterface::ORG_RENEW_EVENT_LOGIN) {
      // Bad way, but class LoginEvent hasn't redirect:
      $url = Url::fromRoute('esia_social_auth_org.request');
      $redirect = new RedirectResponse($url->toString());
      $redirect->send();
    }
  }

}
