<?php

namespace Drupal\esia_social_auth_org\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection): void {
    // Override to check query.
    if ($route = $collection->get('esia_social_auth.callback')) {
      $route->setDefault('_controller', '\Drupal\esia_social_auth_org\Controller\OrgController::callbackOrg');
    }
  }

}
