<?php

namespace Drupal\esia_social_auth_org;

/**
 * Defines the interface for ESIA Organization service.
 */
interface OrgInterface {

  /**
   * Organization renew event.
   */
  const ORG_RENEW_EVENT_REGISTER = 'register';

  const ORG_RENEW_EVENT_LOGIN = 'login';

  /**
   * Get unapproved organization request URL.
   *
   * @param int $uid
   *   User ID.
   *
   * @return string
   *   organization id.
   */
  public function getUnapprovedOrgUrl(int $uid): string;

  /**
   * Get organizations data.
   *
   * @param int $uid
   *   User ID.
   *
   * @return mixed[]
   *   Organizations data.
   */
  public function getOrganizations(int $uid): array;

}
