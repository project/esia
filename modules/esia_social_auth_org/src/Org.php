<?php

namespace Drupal\esia_social_auth_org;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\esia_social_auth\Settings\OrgInterface;
use Drupal\esia_social_auth\Signer;
use Esia\Config;
use Esia\OpenId;

/**
 * ESIA Organization service class.
 */
class Org implements OrgInterface {

  /**
   * The entity type manager service.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current user.
   */
  protected AccountInterface $currentUser;

  /**
   * The config factory.
   */
  protected ConfigFactory $configFactory;

  /**
   * Constructs an org object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Used for accessing configuration object factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $current_user,
    ConfigFactory $config_factory
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnapprovedOrgUrl(int $uid): string {
    $organizations = $this->getOrganizations($uid);
    if (!empty($organizations)) {
      foreach ($organizations as $org) {
        if (empty($org['accessApproved']) || !empty($org['accessUnApproved'])) {
          $prnOid = $org['prnOid'];
          return $this->buildUrl($prnOid);
        }
      }
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizations(int $uid): array {
    try {
      $social_auth_storage = $this->entityTypeManager->getStorage('social_auth');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return [];
    }

    $social_auth_users = $social_auth_storage->loadByProperties([
      'user_id' => $uid,
    ]);

    foreach ($social_auth_users as $social_auth_user) {
      $plugin_id = $social_auth_user->toArray()['plugin_id'][0]['value'];
      if ($plugin_id == 'esia_social_auth') {
        $addData = $social_auth_user->getAdditionalData();
        return $addData['organizations'] ?? [];
      }
    }

    return [];
  }

  /**
   * Build URL.
   */
  private function buildUrl($prnOid): string {
    $main_config = $this->configFactory->get('esia_social_auth.settings');
    $config = $this->configFactory->get('esia_social_auth_org.settings');

    if ($main_config->get('test')) {
      $portalUrl = SocialAuthSettingsInterface::TEST_URL;
    }
    else {
      $portalUrl = SocialAuthSettingsInterface::URL;
    }

    $scopes = explode(',', $config->get('orgScope'));

    $esiaConfig = new Config([
      'clientId' => $main_config->get('client_id'),
      'redirectUrl' => 'https://dev.patentrt.ru/gosuslugi/callback?network=gosuslugi&request_org=' . $prnOid,
      'portalUrl' => $portalUrl,
      'privateKeyPath' => '/tmp',
      'certPath' => '/tmp',
      'scope' => $scopes,
    ]);

    $esiaOpenId = new OpenId($esiaConfig);
    $signer = new Signer($main_config->get('url2signer'));
    $esiaOpenId->setSigner($signer);

    $org_scopes = array_map(function ($scope) use ($prnOid) {
      return 'http://esia.gosuslugi.ru/' . $scope . '?org_oid=' . $prnOid;
    }, $scopes);
//    array_unshift($org_scopes, 'openid');

    $esiaOpenId->getConfig()->setScope($org_scopes);

    $url = $esiaOpenId->buildUrl();

    // $esiaOpenId->getConfig()->setState($parsed['state']);
    /* Force to run session for anon.user:
    $this->request->getSession()->set('gu_state', $parsed['state']);
     */

    return $url;
  }

}
