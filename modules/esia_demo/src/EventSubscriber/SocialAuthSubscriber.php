<?php

namespace Drupal\esia_demo\EventSubscriber;

use Drupal\social_auth\Event\LoginEvent;
use Drupal\social_auth\Event\SocialAuthEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts on Social Auth events.
 */
class SocialAuthSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[SocialAuthEvents::USER_LOGIN] = ['onUserLogin'];

    return $events;
  }

  /**
   * On user login.
   */
  public function onUserLogin(LoginEvent $event): void {
    $account = $event->getDrupalAccount()->getAccount();
    $sa_user = $event->getSocialAuthUser();

    $aData = $sa_user->getAdditionalData();

    $inn = $personInfo['inn'] ?? '';

    $personInfo = $aData['personInfo'] ?? [];
    $fio = '';
    if (!empty($personInfo)) {
      $fio = implode(' ', [
        $personInfo['lastName'] ?? '',
        $personInfo['firstName'] ?? '',
        $personInfo['middleName'] ?? '',
      ]);
    }

    $address = '';
    if (!empty($aData['addressInfo'])) {
      foreach ($aData['addressInfo'] as $item) {
        if ($item['type'] == 'PLV') {
          // Physical address (физический адрес).
          // $item['type'] = 'PRG' - legal address (юридический).
          $address = $this->processAddress($item);
        }
      }
    }

    $mobile = '';
    if (!empty($aData['contactInfo'])) {
      foreach ($aData['contactInfo'] as $item) {
        if ($item['type'] == 'MBT') {
          // Мобильный телефон:
          $mobile = $item['value'];
          break;
        }
      }
    }

    // Next code should be changed due to your requirements.
    // Replace or remove uppercase field names to yours.
    // Additionally, you can add code to compare with the original values to
    // prevent saving unchanged content:
    $account->set('field_FIO', $fio);
    $account->set('field_MOBILE', $mobile);
    $account->set('field_ADDRESS', $address);
    $account->set('field_INN', $inn);
    $account->save();
  }

  /**
   * Process ESIA address to address field.
   *
   * @todo Not yet completed.
   *
   * @param mixed[] $address
   *   Address to be processed.
   *
   * @return mixed[]
   *   Processed address.
   */
  private function processAddress(array $address): array {
    $address_lines = [];

    $country_code = 'RU';

    if (!empty($address['countryId'])) {
      // Bad code:
      $country_code = $this->convertCountryAlphas3To2($address['countryId']);
    }

    $region = '';

    if (!empty($address['fiasCode'])) {
      // You can use daData.ru service to get region by FIAS code.
      // @todo add code that work without dadata.ru.
    }

    if (!empty($address['area'])) {
      $address_lines[] = $address['area'];
    }

    $city = '';

    if (!empty($address['city'])) {
      $city = $address['city'];

      if (!empty($address['settlement'])) {
        $address_lines[] = $address['settlement'];
      }
    }
    else {
      if (!empty($address['settlement'])) {
        $city = $address['settlement'];
      }
    }

    $address_lines[] = $address['street'] ?? '';
    $address_lines[] = $address['house'] ?? '';
    $address_lines[] = $address['flat'] ?? '';
    $address_lines = array_values(array_filter(array_map('trim', $address_lines), 'strlen'));
    $address_line = implode(', ', $address_lines);

    return [
      'country_code' => $country_code,
      'administrative_area' => $region,
      'locality' => $city,
      'postal_code' => $address['zipCode'] ?? '',
      'address_line1' => $address_line,
      'address_line2' => '',
    ];
  }

  /**
   * Country code converter.
   *
   * @param string $code
   *   Country code ISO 3166-1 alpha-3.
   *
   * @return string
   *   Country code ISO 3166-1 alpha-2.
   */
  private function convertCountryAlphas3To2(string $code): string {
    $countries = json_decode('{"AFG":"AF","ALA":"AX","ALB":"AL","DZA":"DZ","ASM":"AS","AND":"AD","AGO":"AO","AIA":"AI","ATA":"AQ","ATG":"AG","ARG":"AR","ARM":"AM","ABW":"AW","AUS":"AU","AUT":"AT","AZE":"AZ","BHS":"BS","BHR":"BH","BGD":"BD","BRB":"BB","BLR":"BY","BEL":"BE","BLZ":"BZ","BEN":"BJ","BMU":"BM","BTN":"BT","BOL":"BO","BIH":"BA","BWA":"BW","BVT":"BV","BRA":"BR","VGB":"VG","IOT":"IO","BRN":"BN","BGR":"BG","BFA":"BF","BDI":"BI","KHM":"KH","CMR":"CM","CAN":"CA","CPV":"CV","CYM":"KY","CAF":"CF","TCD":"TD","CHL":"CL","CHN":"CN","HKG":"HK","MAC":"MO","CXR":"CX","CCK":"CC","COL":"CO","COM":"KM","COG":"CG","COD":"CD","COK":"CK","CRI":"CR","CIV":"CI","HRV":"HR","CUB":"CU","CYP":"CY","CZE":"CZ","DNK":"DK","DKK":"DK","DJI":"DJ","DMA":"DM","DOM":"DO","ECU":"EC","Sal":"El","GNQ":"GQ","ERI":"ER","EST":"EE","ETH":"ET","FLK":"FK","FRO":"FO","FJI":"FJ","FIN":"FI","FRA":"FR","GUF":"GF","PYF":"PF","ATF":"TF","GAB":"GA","GMB":"GM","GEO":"GE","DEU":"DE","GHA":"GH","GIB":"GI","GRC":"GR","GRL":"GL","GRD":"GD","GLP":"GP","GUM":"GU","GTM":"GT","GGY":"GG","GIN":"GN","GNB":"GW","GUY":"GY","HTI":"HT","HMD":"HM","VAT":"VA","HND":"HN","HUN":"HU","ISL":"IS","IND":"IN","IDN":"ID","IRN":"IR","IRQ":"IQ","IRL":"IE","IMN":"IM","ISR":"IL","ITA":"IT","JAM":"JM","JPN":"JP","JEY":"JE","JOR":"JO","KAZ":"KZ","KEN":"KE","KIR":"KI","PRK":"KP","KOR":"KR","KWT":"KW","KGZ":"KG","LAO":"LA","LVA":"LV","LBN":"LB","LSO":"LS","LBR":"LR","LBY":"LY","LIE":"LI","LTU":"LT","LUX":"LU","MKD":"MK","MDG":"MG","MWI":"MW","MYS":"MY","MDV":"MV","MLI":"ML","MLT":"MT","MHL":"MH","MTQ":"MQ","MRT":"MR","MUS":"MU","MYT":"YT","MEX":"MX","FSM":"FM","MDA":"MD","MCO":"MC","MNG":"MN","MNE":"ME","MSR":"MS","MAR":"MA","MOZ":"MZ","MMR":"MM","NAM":"NA","NRU":"NR","NPL":"NP","NLD":"NL","ANT":"AN","NCL":"NC","NZL":"NZ","NIC":"NI","NER":"NE","NGA":"NG","NIU":"NU","NFK":"NF","MNP":"MP","NOR":"NO","OMN":"OM","PAK":"PK","PLW":"PW","PSE":"PS","PAN":"PA","PNG":"PG","PRY":"PY","PER":"PE","PHL":"PH","PCN":"PN","POL":"PL","PRT":"PT","PRI":"PR","QAT":"QA","REU":"RE","ROU":"RO","RUS":"RU","RWA":"RW","BLM":"BL","SHN":"SH","KNA":"KN","LCA":"LC","MAF":"MF","SPM":"PM","VCT":"VC","WSM":"WS","SMR":"SM","STP":"ST","SAU":"SA","SEN":"SN","SRB":"RS","SYC":"SC","SLE":"SL","SGP":"SG","SVK":"SK","SVN":"SI","SLB":"SB","SOM":"SO","ZAF":"ZA","SGS":"GS","SSD":"SS","ESP":"ES","LKA":"LK","SDN":"SD","SUR":"SR","SJM":"SJ","SWZ":"SZ","SWE":"SE","CHE":"CH","SYR":"SY","TWN":"TW","TJK":"TJ","TZA":"TZ","THA":"TH","TLS":"TL","TGO":"TG","TKL":"TK","TON":"TO","TTO":"TT","TUN":"TN","TUR":"TR","TKM":"TM","TCA":"TC","TUV":"TV","UGA":"UG","UKR":"UA","ARE":"AE","GBR":"GB","USA":"US","UMI":"UM","URY":"UY","UZB":"UZ","VUT":"VU","VEN":"VE","VNM":"VN","VIR":"VI","WLF":"WF","ESH":"EH","YEM":"YE","ZMB":"ZM","ZWE":"ZW","GBP":"GB","RUB":"RU","NOK":"NO"}', TRUE);

    return $countries[$code] ?? '';
  }

}
