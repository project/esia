<?php

namespace Drupal\esia_social_auth\Settings;

use Drupal\social_auth\Settings\SettingsInterface;

/**
 * Defines the settings interface.
 */
interface SocialAuthSettingsInterface extends SettingsInterface {

  /**
   * ESIA URLs.
   */
  const URL = 'https://esia.gosuslugi.ru/';
  const TEST_URL = 'https://esia-portal1.test.gosuslugi.ru/';

  /**
   * Get Portal URL.
   *
   * @return string
   *   The group relation type manager.
   */
  public function getUrl2Signer(): string;

  /**
   * Is test server?
   *
   * @return bool
   *   The group relation type manager.
   */
  public function isTest(): bool;

  /**
   * Get URL to Esia.
   */
  public function getUrl2Esia(): string;

}
