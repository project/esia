<?php

namespace Drupal\esia_social_auth\Settings;

use Drupal\social_auth\Settings\SettingsBase;

/**
 * Defines methods to get Social Auth ESIA app settings.
 */
class SocialAuthSettings extends SettingsBase implements SocialAuthSettingsInterface {

  /**
   * Path to key.
   */
  protected string $url2signer;

  /**
   * Test.
   */
  protected bool $test;

  /**
   * {@inheritdoc}
   */
  public function getUrl2Signer(): string {
    if (!isset($this->url2signer)) {
      $this->url2signer = $this->config->get('url2signer');
    }

    return $this->url2signer;
  }

  /**
   * {@inheritdoc}
   */
  public function isTest(): bool {
    if (!isset($this->test)) {
      $this->test = (bool) $this->config->get('test');
    }

    return $this->test;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl2Esia(): string {
    if ($this->isTest()) {
      $url = SocialAuthSettingsInterface::TEST_URL;
    }
    else {
      $url = SocialAuthSettingsInterface::URL;
    }

    return $url;
  }

}
