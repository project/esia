<?php

namespace Drupal\esia_social_auth;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\social_auth\AuthManager\OAuth2Manager;
use Drupal\social_auth\User\SocialAuthUser;
use Drupal\social_auth\User\SocialAuthUserInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Contains all the logic for ESIA authentication.
 */
class SocialAuthManager extends OAuth2Manager {

  /**
   * ESIA client.
   *
   * @var \Esia\OpenId
   */
  protected mixed $client;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Used for accessing configuration object factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Used to get the authorization code from the callback request.
   */
  public function __construct(
    ConfigFactory $configFactory,
    LoggerChannelFactoryInterface $logger_factory,
    RequestStack $request_stack
  ) {
    parent::__construct(
      $configFactory->get('esia_social_auth.settings'),
      $logger_factory,
      $this->request = $request_stack->getCurrentRequest());
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(): void {
    try {
      $code = (string) $this->request->query->get('code');
      $token = $this->client->getToken($code);

      $this->setAccessToken($token);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('esia_social_auth')
        ->error('There was an error during authentication. Exception: ' . $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUserInfo(): SocialAuthUserInterface {
    $access_token = $this->getAccessToken();

    if (!$this->user && $access_token !== NULL) {
      $personInfo = $this->client->getPersonInfo();
      $addressInfo = $this->client->getAddressInfo();
      $contactInfo = $this->client->getContactInfo();
      $documentInfo = $this->client->getDocInfo();

      $firstName = $personInfo['firstName'] ?? '';
      $lastName = $personInfo['lastName'] ?? '';

      $email = '';

      if (!empty($contactInfo)) {
        foreach ($contactInfo as $item) {
          if ($item['type'] == 'EML') {
            $email = $item['value'];
            break;
          }
        }
      }

      if ($email) {
        $name = substr($email, 0, strrpos($email, '@'));
      }
      else {
        $name = implode(' ', [$firstName, $lastName]);
      }

      $this->user = new SocialAuthUser(
        $name,
        $personInfo['oid'],
        $this->getAccessToken(),
        $email,
        NULL,
        $this->getExtraDetails()
      );
      $this->user->setFirstName($firstName);
      $this->user->setLastName($lastName);

      $this->client->getConfig()->setOid($personInfo['oid']);

      $info = [
        'personInfo' => $personInfo,
        'addressInfo' => $addressInfo,
        'contactInfo' => $contactInfo,
        'documentInfo' => $documentInfo,
      ];

      $this->user->setAdditionalData($info);
    }

    return $this->user;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl(): string {
    $url = $this->client->buildUrl();
    parse_str($url, $parsed);
    $this->state = $parsed['state'];

    $this->client->getConfig()->setState($this->state);

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function requestEndPoint(string $method, string $path, ?string $domain = NULL, array $options = []): mixed {
    if (!$domain) {
      $domain = $this->client->getConfig()->getPortalUrl();
    }

    $url = $domain . $path;

    try {
      $token = $this->getAccessToken();
      $options['headers']['Authorization'] = 'Bearer ' . $token;

      $request = $this->httpClient()->request($method, $url, $options);

      return json_decode($request->getBody()->getContents(), TRUE) ?: [];
    }
    catch (RequestException $e) {
      watchdog_exception('esia_social_auth', $e);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setState(): string {
    return $this->client->getConfig()->setState();
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): string {
    return $this->client->getConfig()->getState();
  }

  /**
   * Returns the default http client.
   *
   * @return object
   *   A guzzle http client instance.
   */
  public static function httpClient(): object {
    // Is there another way?
    return \Drupal::getContainer()->get('http_client');
  }

}
