<?php

namespace Drupal\esia_social_auth\Plugin\Network;

use Drupal\esia\Signer;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth\Plugin\Network\NetworkInterface;
use Esia\Config;
use Esia\OpenId;

/**
 * Defines a Network Plugin for ESIA Social Auth.
 *
 * @Network(
 *   id = "esia_social_auth",
 *   short_name = "esia",
 *   social_network = "ESIA",
 *   img_path = "images/gosuslugiID_znak+text_M_blue.svg",
 *   type = "social_auth",
 *   class_name = "\Esia\OpenId",
 *   auth_manager = "\Drupal\esia_social_auth\SocialAuthManager",
 *   routes = {
 *     "redirect": "social_auth.network.redirect",
 *     "callback": "esia_social_auth.callback",
 *     "settings_form": "social_auth.network.settings_form",
 *   },
 *   handlers = {
 *     "settings": {
 *       "class":"\Drupal\esia_social_auth\Settings\SocialAuthSettings",
 *       "config_id": "esia_social_auth.settings"
 *     }
 *   }
 * )
 */
class SocialAuth extends NetworkBase implements NetworkInterface {

  /**
   * {@inheritdoc}
   */
  protected function initSdk(): mixed {
    $network = $this->networkManager->getDefinition($this->pluginId);

    if (!class_exists($network['class_name'])) {
      throw new SocialApiException("Library class not found: {$network['class_name']}");
    }

    if ($this->validateConfig($this->settings)) {
      $scopes = ['email'];

      $extra_scopes = $this->settings->getConfig()->get('scopes');

      if ($extra_scopes) {
        $scopes = array_merge($scopes, explode(',', $extra_scopes));
      }

      $config = new Config([
        'clientId' => $this->settings->getClientId(),
        'redirectUrl' => $this->getCallbackUrl()->setAbsolute()->toString(),
        'portalUrl' => $this->settings->getUrl2Esia(),
        'privateKeyPath' => '/tmp',
        'certPath' => '/tmp',
        'scope' => $scopes,
      ]);
      $esia = new OpenId($config);

      $signer = new Signer($this->settings->getUrl2Signer());
      $esia->setSigner($signer);

      return $esia;
    }

    throw new SocialApiException('ESIA configuration validation failed -- verify settings.');
  }

}
