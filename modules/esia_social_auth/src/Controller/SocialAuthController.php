<?php

namespace Drupal\esia_social_auth\Controller;

use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\Plugin\Network\NetworkInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Social Auth ESIA routes.
 */
class SocialAuthController extends OAuth2ControllerBase {

  /**
   * Callback response router handler for networks.
   */
  public function callback(NetworkInterface $network): RedirectResponse {
    $plugin_id = "esia_social_auth";

    if ($this->networkManager->hasDefinition($plugin_id)) {
      $network = $this->networkManager->createInstance($plugin_id);
      return parent::callback($network);
    }
  }

}
