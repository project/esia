<?php

namespace Drupal\esia_social_auth\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Settings form for ESIA.
 */
class SocialAuthSettings extends SocialAuthSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_auth_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return array_merge(
      parent::getEditableConfigNames(),
      ['esia_social_auth.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NetworkInterface $network = NULL): array {
    $network = $this->networkManager->createInstance('esia_social_auth');
    $form = parent::buildForm($form, $form_state, $network);

    $config = $this->config('esia_social_auth.settings');

    $form['network']['url2signer'] = [
      '#title' => $this->t('URL to Signer service'),
      '#type' => 'textfield',
      '#default_value' => $config->get('url2signer'),
      '#weight' => 3,
      '#required' => TRUE,
      '#description' => $this->t('Sample: <code>http://127.0.0.1:3037/cryptopro</code>'),
    ];

    $form['network']['test'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Test'),
      '#default_value' => $config->get('test'),
      '#weight' => 4,
    ];

    $form['network']['advanced']['#weight'] = 5;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    $this->config('esia_social_auth.settings')
      ->set('client_id', trim($values['client_id']))
      ->set('client_secret', trim($values['client_secret']))
      ->set('scopes', $values['scopes'])
      ->set('url2signer', $values['url2signer'])
      ->set('endpoints', $values['endpoints'])
      ->set('test', $values['test'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
