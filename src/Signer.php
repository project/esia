<?php

namespace Drupal\esia;

use Esia\Signer\SignerInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Signer for Cryptopro docker.
 */
class Signer implements SignerInterface {

  /**
   * Base url.
   */
  protected string $baseUrl;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $baseUrl) {
    $this->baseUrl = $baseUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function sign(string $message): string {
    $result = '';

    try {
      $options = [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'body' => json_encode(['text' => $message]),
      ];

      $url = $this->baseUrl . '/sign';

      $httpClient = \Drupal::httpClient();
      $request = $httpClient->request('POST', $url, $options);

      $response_result = json_decode($request->getBody()->getContents(), TRUE) ?: [];

      $result = $response_result['result'] ?? '';
    }
    catch (RequestException $e) {
      watchdog_exception('esia', $e);
    }

    return $result;
  }

}
